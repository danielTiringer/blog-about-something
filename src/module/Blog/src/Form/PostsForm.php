<?php

namespace Blog\Form;

use Laminas\Form\Form;

class PostsForm extends Form
{
    public function __construct($name = null)
    {
        parent::__construct('posts');

        $this->add([
            'name' => 'id',
            'type' => 'hidden',
        ]);
        $this->add([
            'name' => 'author',
            'type' => 'text',
            'options' => [
                'label' => 'Author',
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        $this->add([
            'name' => 'title',
            'type' => 'text',
            'options' => [
                'label' => 'Title',
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        $this->add([
            'name' => 'body',
            'type' => 'textarea',
            'options' => [
                'label' => 'Body',
            ],
            'attributes' => [
                'class' => 'form-control',
            ],
        ]);
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Submit',
                'class' => 'btn btn-primary',
            ],
        ]);
    }
}
