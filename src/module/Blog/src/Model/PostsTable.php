<?php

namespace Blog\Model;

use Laminas\Db\TableGateway\TableGatewayInterface;

class PostsTable
{
    private $tableGateway;

    public function __construct(TableGatewayInterface $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function findAll()
    {
        return $this->tableGateway->select();
    }

    public function getPost(int $id)
    {
        $query = $this->tableGateway->select(['id' => $id]);
        return $query->current();
    }

    public function savePost(Blog $post)
    {
        $data = [
            'author' => $post->author,
            'title'  => $post->title,
            'body' => $post->body,
        ];

        if (!$post->id) {
            $this->tableGateway->insert($data);
        }
        $this->tableGateway->update($data, ['id' => $post->id]);
    }

    public function deletePost(int $id)
    {
        $this->tableGateway->delete(['id' => $id]);
    }
}
