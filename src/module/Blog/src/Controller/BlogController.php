<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */

declare(strict_types=1);

namespace Blog\Controller;

use Blog\Form\PostsForm;
use Blog\Model\Blog;
use Blog\Model\PostsTable;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class BlogController extends AbstractActionController
{
    private $table;

    public function __construct(PostsTable $table)
    {
        $this->table = $table;
    }

    public function indexAction()
    {
        return new ViewModel([
            'posts' => $this->table->findAll(),
        ]);
    }

    public function addAction()
    {
        $form = new PostsForm();

        $request = $this->getRequest();

        if ($request->isGet()) {
            return ['form' => $form];
        }

        $post = new Blog();

        $form->setInputFilter($post->getInputFilter());
        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return ['form' => $form];
        }

        $post->exchangeArray($form->getData());

        $this->table->savePost($post);

        return $this->redirect()->toRoute('blog');
    }

    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $post = $this->table->getPost((int)$id);
        $request = $this->getRequest();

        $form = new PostsForm();
        $form->bind($post);

        if ($request->isGet()) {
            return ['id' => $id, 'form' => $form];
        }

        $form->setInputFilter($post->getInputFilter());
        $form->setData($request->getPost());

        if (!$form->isValid()) {
            return ['form' => $form];
        }

        $this->table->savePost($post);

        return $this->redirect()->toRoute('blog');
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        $id = $request->getPost()->id;

        $this->table->deletePost((int)$id);

        return $this->redirect()->toRoute('blog');
    }
}
